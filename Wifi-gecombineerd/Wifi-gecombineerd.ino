// Hardware: Arduino + ESP8266 module
//
// Ps. zet de ESP module eerst op 9600 baud met het commando:
// bij <= AT v0.40, commando: AT+CIOBAUD=9600
// bij >  AT v0.40, commando: AT+UART_DEF=9600,8,1,0,0
// dit omdat de softwareserial anders niet goed werkt met snelheden van 115200 baud!

#include "DHT.h"
#define DHTPIN 4            // Welke pin we gebruiken voor luchtvochtigheid/temp sensor
#define DHTTYPE DHT22       // DHT 22  (AM2302), AM2321
#define KALIBRATIEWAARDETEMP 0 // Kalibratiewaarde voor de temperatuursensor in graden celcius
#define KALIBRATIEWAARDEVOCHT 0 // Kalibratiewaarde voor de luchtvochtigheidssensor in %

DHT dht(DHTPIN, DHTTYPE);
 
#include <SoftwareSerial.h>
SoftwareSerial esp8266(2,3); // RX, TX
 
void setup() {
  Serial.begin(9600); // Start de seriele monitor op 9600 baud.
  esp8266.begin(9600); // Start de software monitor op 9600 baud.
 
  // WiFi instellingen SSID en wachtwoord.
  String SSIDstring = ("\"PWSWeerstation\"");
  String PASSstring = ("\"watishetweer\"");
  
  Serial.println("\r\n----- [ RESET DE MODULE (RST) ] -----");  
  sendData("AT+RST\r\n", 1000, true); // Reset de ESP module.
 
  Serial.println("\r\n----- [ ZET ESP IN AP MODE (CWMODE) ] -----");  
  sendData("AT+CWMODE=2\r\n", 500, true); // Configureer de ESP in "station mode" (1=Station, 2=AP, 3=Station+AP).
 
  Serial.println("\r\n----- [ MAAK WIFI NETWERK AAN (CWSAP) ] -----");
  sendData("AT+CWSAP=" + SSIDstring + "," + PASSstring + ",1,4\r\n", 5000, true); // Inloggen op de WiFi met wachtwoord.
 
  Serial.println("\r\n----- [ MULTIPLEX MODE OP MULTIPLE CONNECTIES ZETTEN (CIPMUX) ] -----");
  // Zet multiplex in "multiple mode, zo kan de server meerdere verbindingen accepteren, dit is nodig om de server te starten.
  sendData("AT+CIPMUX=1\r\n", 500, true);
 
  Serial.println("\r\n----- [ SERVER STARTEN (CIPSERVER) ] -----"); 
  sendData("AT+CIPSERVER=1,80\r\n", 500, true); // Zet de server actief op poort 80.
  
  Serial.println("\r\n----- [IP ADRES] -----");
  sendData("AT+CIFSR\r\n", 500, true); // Geef het verkregen IP adres weer.

  dht.begin();             // Start de luchtvochtigheid/temp sensor
  delay(2000);             // Wacht tot sensor is opgestart
}
 
void loop() {
  // Het lezen vand e sensor duurt ongeveer 250 milliseconden!
  float h = dht.readHumidity() + KALIBRATIEWAARDEVOCHT;
  float t = dht.readTemperature() + KALIBRATIEWAARDETEMP;

  // Check of het uitlezen heeft gefaald, zoja, stop (om later opnieuw te proberen).
  if (isnan(h) || isnan(t)) {
    Serial.println("Luchtvochtigheid/Temp sensor niet uitgelezen!");
    return;
  }

  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);
  
  if(esp8266.available()) { // Controleer op de ESP een bericht stuurt.
    if(esp8266.find("+IPD,")) {
      delay(1000);
      int connectionId = esp8266.read() - 48; // Haal er 48 van af, want de "read()" functie geeft een ASCII decimaal waaarde en 0 terug (het eerste decimale nummer begint op 48)
 
      // ----- WEBPAGINA -----
      String Webpagina = "<head><meta http-equiv=\"refresh\" content=\"5\"></head><body><h1>Weerstation</h1><p><b>Luchtvochtigheid:</b> " + (String)h + "%</p><p><b>Temperatuur:</b> " + (String)t + "&deg;C</p><p><b>Gevoelstemperatuur:</b> " + (String)hic + "&deg;C</p><p><small>Gemaakt door: Gijs van Barneveld, Lars Uijlenhoet, Sami Chaoui</small></p></body>";
      // ----- WEBPAGINA -----
 
      sendData("AT+CIPSEND=" + String(connectionId) + "," + Webpagina.length() + "\r\n", 500, true);
      sendData(Webpagina, 1000, true); // Stuur de webpagina door.
      sendData("AT+CIPCLOSE=" + String(connectionId) + "\r\n", 1000, true); // Sluit de connectie.
    }
  }
}
 
String sendData(String command, const int timeout, boolean debug) {
  String response = "";
  esp8266.print(command); // Stuur een "lees" karakter naar de ESP.
  long int time = millis();
  while( (time+timeout) > millis()) {
    while(esp8266.available()) { // De ESP heeft data om weer te geven, laat het zien in de serial monitor. 
      char c = esp8266.read(); // Lees het volgende karakter.
      response+=c;
    }
  }
  if(debug) { Serial.print(response); } 
  return response;
}
