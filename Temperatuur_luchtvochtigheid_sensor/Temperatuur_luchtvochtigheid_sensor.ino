#include "DHT.h"
#define DHTPIN 4            // Welke pin we gebruiken voor luchtvochtigheid/temp sensor
#define DHTTYPE DHT22       // DHT 22  (AM2302), AM2321

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);      // open the serial port at 9600 bps
  dht.begin();             // Start de luchtvochtigheid/temp sensor
  delay(2000);             // Wacht tot sensor is opgestart
}

void loop() {
  // Het lezen vand e sensor duurt ongeveer 250 milliseconden!
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  // Check of het uitlezen heeft gefaald, zoja, stop (om later opnieuw te proberen).
  if (isnan(h) || isnan(t)) {
    Serial.println("Luchtvochtigheid/Temp sensor niet uitgelezen!");
    return;
  }

  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);
  
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C\t");
  Serial.print("Heat index: ");
  Serial.print(hic);
  Serial.println(" *C");
}


